<%-- 
    Document   : listarFuncionarios
    Created on : 15/03/2017, 20:34:12
    Author     : Aline
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--Metodo Post: Chama a ação listar direto do menu -->
<title>Listar todos funcionários</title>
</head>
<body>
    <p><a href="index.html">Voltar</a></p>
    <table border=0>
        <thead>
            <tr>
                <th>Id</th>
                <th>Nome</th>
                <th>Idade</th>
                <th>Telefone</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${listaFuncionarios}" var="funcionario">
                <tr>
                    <td><c:out value="${funcionario.id}" /></td>
                    <td><c:out value="${funcionario.nome}" /></td>
                    <td><c:out value="${funcionario.idade}" /></td>                    
                    <td><c:out value="${funcionario.telefone}" /></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    
</body>
</html>