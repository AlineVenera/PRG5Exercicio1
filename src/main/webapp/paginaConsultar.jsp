<%-- 
    Document   : listarFuncionarios
    Created on : 15/03/2017, 20:27:15
    Author     : Aline
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Consulta de funcionários</title>
</head>
<body>
    <p><a href="index.html">Voltar</a></p>
    
    <!--Metodo Post: Chama a ação consultar ao clicar em submit -->
    <div>        
        <form method="POST" action='Consultar' name="formConsultar">        
            Id : <input type="text" name="id" value="" /> 
            Nome : <input type="text" name="nome" value="" /> 
            <input type="submit" value="Buscar" />
        </form>           
    </div>
    
    <hr>
    
    <table border=0>
        <thead>
            <tr>
                <th>Id</th>
                <th>Nome</th>
                <th>Idade</th>
                <th>Telefone</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${listaFuncionarios}" var="funcionario">
                <tr>
                    <!-- Passagem de parâmeros -->
                    <td><c:out value="${funcionario.id}" /></td>
                    <td><c:out value="${funcionario.nome}" /></td>
                    <td><c:out value="${funcionario.idade}" /></td>                    
                    <td><c:out value="${funcionario.telefone}" /></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    
</body>
</html>