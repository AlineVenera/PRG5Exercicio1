<%-- 
    Document   : listarFuncionarios
    Created on : 15/03/2017, 20:35:55
    Author     : Aline
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Inserir funcionário</title>
</head>
<body>
    <p><a href="index.html">Voltar</a></p>
    
    <!--Metodo Post: Chama a ação de inserir ao clicar em submit -->
    <form method="POST" action='Inserir' name="formAdicionarFuncionario">
        <table>
            <tr>
                <td>
                    Nome : 
                </td>
                <td>
                    <input type="text" name="nome"
                        value="<c:out value="${funcionario.nome}" />" />
                </td>
            </tr>
            <tr>
                <td>
                    Idade : 
                </td>
                <td>
                    <input type="text" name="idade"
                        value="<c:out value="${funcionario.idade}" />" />
                </td>
            </tr>
            <tr>
                <td>
                    Telefone : 
                </td>
                <td>
                    <input type="text" name="telefone"
                        value="<c:out value="${funcionario.telefone}" />" />
                </td>
            </tr>
        </table>
        <input type="submit" value="Salvar" />
    </form>
</body>
</html>