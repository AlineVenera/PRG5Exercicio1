<%-- 
    Document   : listarFuncionarios
    Created on : 15/03/2017, 20:27:15
    Author     : Aline
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!--Quando o cadastro é efetivado -->
<title>Funcionário cadastrado com sucesso</title>
</head>
<body>
    <p>Funcionário cadastrado com sucesso!</p>
    <p><a href="index.html">Voltar</a></p>
    <p><a href="paginaInserir.jsp">Inserir</a></p>
</body>
</html>