/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.DAO;
import DTO.DTO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

/**
 *
 * @author Aline
 * Trata as requisições recebidas de clientes Web (nav)
 */
@WebServlet(name = "Inserir", urlPatterns = {"/Inserir"})
public class InserirFuncionarioServlet extends HttpServlet{

    //-- Faz o insert conforme os dados inseridos no form
    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        DAO dao = new DAO();
        DTO fun = new DTO();
        fun.setNome(req.getParameter("nome"));
        fun.setIdade(Integer.parseInt(req.getParameter("idade")));
        fun.setTelefone(req.getParameter("telefone"));
        dao.inserirFuncionario(fun);
        req.getRequestDispatcher("paginaSucesso.jsp").forward(req, res);
    }
}
