/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.DAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

/**
 *
 * @author Aline
 * Trata as requisições recebidas de clientes Web (nav)
 */
@WebServlet(name = "Consultar", urlPatterns = {"/Consultar"})
public class ConsultarFuncionariosServlet extends HttpServlet {

    //-- Pega os parâmetros vindo da tela de consulta e faz a consulta por eles
    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        DAO dao = new DAO();
        String parametroId = req.getParameter("id");
        Long id = null != parametroId && !parametroId.isEmpty() ? Long.valueOf(parametroId) : 0l;
        req.setAttribute("listaFuncionarios", dao.consultaFuncionariosComParametros(id, req.getParameter("nome")));
        req.getRequestDispatcher("paginaConsultar.jsp").forward(req, res);
    }
}
