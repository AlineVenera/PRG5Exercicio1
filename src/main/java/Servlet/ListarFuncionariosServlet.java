/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import DAO.DAO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

/**
 *
 * @author Aline
 * Trata as requisições recebidas de clientes Web (nav)
 */
@WebServlet(name = "Listar", urlPatterns = {"/Listar"})
public class ListarFuncionariosServlet extends HttpServlet{

    //-- Faz a pesquisa de todos os funcionários (sem necessidade de param)
    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        DAO dao = new DAO();
        req.setAttribute("listaFuncionarios", dao.listaFuncionarios());
        req.getRequestDispatcher("paginaPrincipal.jsp").forward(req, res);
    }
}
