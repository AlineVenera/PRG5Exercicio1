/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Conexao.Conexao;
import DTO.DTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aline
 * Persistência no banco de dados (inclusão/exclusão/alteração)
 */
public class DAO {

    public ArrayList listaFuncionarios() {
        ArrayList<DTO> funcionarios = new ArrayList();
        try {
            Connection con = Conexao.getConexao();
            String sql = "select * from funcionario;";
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                funcionarios.add(new DTO(rs.getInt("id"),
                        rs.getString("nome"),
                        rs.getInt("idade"),
                        rs.getString("telefone")
                ));
            }
            rs.close();
            ps.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return funcionarios;
    }

    public ArrayList consultaFuncionariosComParametros(Long id, String nome) {
        ArrayList<DTO> funcionarios = new ArrayList();
        try {
            nome = null != nome && !nome.isEmpty() ? "%" + nome + "%" : " ";
            Connection con = Conexao.getConexao();
            String sql = " select * from funcionario where id = ? or nome like ? ";
            
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setLong(1, id);
            ps.setString(2, nome);
            
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                funcionarios.add(new DTO(rs.getInt("id"),
                        rs.getString("nome"),
                        rs.getInt("idade"),
                        rs.getString("telefone")
                ));
            }
            rs.close();
            ps.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return funcionarios;
    }
    
    public static void inserirFuncionario(DTO fun) {
        try {
            Connection con = Conexao.getConexao();
            String sql = "insert into funcionario(nome,idade,telefone) values(?,?,?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, fun.getNome());
            ps.setInt(2, fun.getIdade());
            ps.setString(3, fun.getTelefone());
            ps.executeUpdate();
            ps.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*public DTO consultaFuncionarioPorCodigo(Long pesquisa) {
        DTO funcionario = new DTO();
        try {
            Connection con = Conexao.getConexao();
            String sql = "select * from funcionario where id = ?;";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setLong(1, pesquisa);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                funcionario.setId(rs.getInt("id"));
                funcionario.setNome(rs.getString("nome"));
                funcionario.setIdade(rs.getInt("idade"));
                funcionario.setTelefone(rs.getString("telefone"));
            }
            rs.close();
            ps.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return funcionario;
    }*/
}
